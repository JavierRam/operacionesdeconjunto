﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace operacionDeConjuntos
{
    class Program
    {
        static void Main(string[] args)
        {

            //Creando los conjuntos con A y B con Hashset
            HashSet<int> NumA = new HashSet<int>();
            HashSet<int> NumB = new HashSet<int>();

            //Alimentando los datos desde el teclado y almacenandolos en el conjunto A con el método add
            Console.WriteLine("Ingresando los valores a los conjuntos A y B");
            Console.WriteLine("Ingresa los 7 valores al conjunto A");
            int c = 0;
            for (int i = 0; i < 7; i++)
            {
                Console.Write("[{0}]", c = c + 1);
                int var = int.Parse(Console.ReadLine());
                NumA.Add(var);
            }

            //Alimentando los datos desde el teclado y almacenandolos en el conjunto B con el método add
            Console.WriteLine("Ingresa los 5 valores al conjunto A");
            int y = 0;
            for (int i = 0; i < 5; i++)
            {
                Console.Write("[{0}]", y = y + 1);
                int var = int.Parse(Console.ReadLine());
                NumB.Add(var);
            }

            //Se crea el conjunto union1  que va a contener la union de los conjunto A y B
            HashSet<int> Union1 = new HashSet<int>(NumA);
            Union1.UnionWith(NumB);

            Console.Write("Conjunto A = {");
            foreach (int i in NumA)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Conjunto B = {");
            foreach (int i in NumB)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Union = {");
            foreach (int i in Union1)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");
            Console.WriteLine('\n');

            //Se crea el conjunto de interseccion  que va a contener la union de los conjunto A y B
            HashSet<int> Interseccion = new HashSet<int>(NumA);
            Interseccion.IntersectWith(NumB);

            Console.Write("Conjunto A = {");
            foreach (int i in NumA)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Conjunto B = {");
            foreach (int i in NumB)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Interseccion = {");
            foreach (int i in Interseccion)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");
            Console.WriteLine('\n');

            //Se crea el conjunto de Diferencia  que va a contener la union de los conjunto A y B
            HashSet<int> Diferencia = new HashSet<int>(NumA);
            Diferencia.ExceptWith(NumB);

            Console.Write("Conjunto A = {");
            foreach (int i in NumA)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Conjunto B = {");
            foreach (int i in NumB)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Diferencia = {");
            foreach (int i in Diferencia)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");
            Console.WriteLine('\n');

            //Se crea el conjunto de Diferencia  que va a contener la union de los conjunto A y B
            HashSet<int> DifSimetrica = new HashSet<int>(NumA);
            DifSimetrica.SymmetricExceptWith(NumB);

            Console.Write("Conjunto A = {");
            foreach (int i in NumA)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("Conjunto B = {");
            foreach (int i in NumB)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");

            Console.Write("DifSimetrica = {");
            foreach (int i in DifSimetrica)
            {
                Console.Write("{0}", i);
            }
            Console.WriteLine("}");
            Console.WriteLine('\n');

        }
    }
}